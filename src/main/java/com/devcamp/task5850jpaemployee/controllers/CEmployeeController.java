package com.devcamp.task5850jpaemployee.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5850jpaemployee.model.CEmployee;
import com.devcamp.task5850jpaemployee.repository.iEmployeeRepository;

@RestController
@CrossOrigin
public class CEmployeeController {
    @Autowired
    iEmployeeRepository employeeRepository;
    @GetMapping("/employeeList")
    public ResponseEntity<List<CEmployee>> getVouchers(){
        try {
            List<CEmployee> listEmployees = new ArrayList<CEmployee>();
            employeeRepository.findAll().forEach(listEmployees::add);
            if (listEmployees.size() ==0){
                return new ResponseEntity<>(listEmployees, HttpStatus.NOT_FOUND);
            }
            else return new ResponseEntity<>(listEmployees, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
