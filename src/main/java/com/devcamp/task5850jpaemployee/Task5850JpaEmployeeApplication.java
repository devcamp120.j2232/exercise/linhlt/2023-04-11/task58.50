package com.devcamp.task5850jpaemployee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5850JpaEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5850JpaEmployeeApplication.class, args);
	}

}
