package com.devcamp.task5850jpaemployee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5850jpaemployee.model.CEmployee;

public interface iEmployeeRepository extends JpaRepository<CEmployee, Long>{
    
}
